## Open Source & Communities
*-Jeremy Meiss*<br />
![conflogo](images/kszwbimzbc2wnlor7say.jpg)
---
## Who am I
![ARGH](images/IMG_Jeremy_2014.JPG)
#### Jeremy Meiss
Technologist & Community-building Geek<br />
<i class="fa fa-twitter"></i> @jerdogxda<br />
<i class="fa fa-web"></i>https://jmeiss.me<br />
<i class="fa fa-email"></i>jeremy.meiss@gmail

+++
## More about Me
- 20+ years in Technology
- 16 years in Mobile <!-- .element: class="fragment" -->
- 10 years at XDA-Developers in Community & Developer Relations <!-- .element: class="fragment" -->
- Community/DevRel Consultant <!-- .element: class="fragment" -->

+++
## More than you probably wanted to know
- Wife and 3 kids
- Used to own a coffee shop <!-- .element: class="fragment" -->
- Play guitar and percussion <!-- .element: class="fragment" -->
- A Whovian <!-- .element: class="fragment" -->
- Unable to do tricks <!-- .element: class="fragment" -->

---
## Overview
#### What is open source?
#### Advantages of open source <!-- .element: class="fragment" -->
#### Community-building in open source <!-- .element: class="fragment" -->

Note:
So today I am going to touch a bit on what open source is, why it can be an advantage to businesses and developers alike, and then touch on Community-building steps, and finish up with a few things about XDA-Developers, the community I have spent the most time with these past 10 years or so. Are we ready?

+++
![doctorwho](images/TheDoctor_cool.gif)

---
### What is open source?
"software that can be freely used, changed, and shared (in modified or unmodified form) by anyone"<br />
*- Open Source Initiative (opensource.org)*

Note:
The Open Source Initiative defines Open Source as....

+++
![brilliant](images/brilliant.gif)

+++
#### Open source is growing exponentially
- 23 million open source developers worldwide <!-- .element: class="fragment" -->
- 22 million accounts and 64 million repositories on GitHub <!-- .element: class="fragment" -->
- 41 million lines of code <!-- .element: class="fragment" -->
- 1,100 new open source projects every day <!-- .element: class="fragment" -->
- 10,000 new versions of open source projects every day <!-- .element: class="fragment" -->

*-Open Source Summit 2017, The Linux Foundation* <!-- .element: class="fragment" -->

Note:
According to the Linux Foundation at the Open Source Summit held in September, the following stats are very impressive.

+++?image=images/topgear_driving.gif&size=auto 90%

---
### Advantages of open source
#### Company Cost Savings
- Shift devs from low-value to high-value work <!-- .element: class="fragment" -->
- Lower TCO <!-- .element: class="fragment" -->
- Modern SW Dev Practices <!-- .element: class="fragment" -->
  - Electronic (open process) <!-- .element: class="fragment" -->
  - Asynchronous activity <!-- .element: class="fragment" -->
  - Rapid testing <!-- .element: class="fragment" -->

Note:
Open source offers a number of cost savings for companies and developers. [**Shift devs**] You don't need to reinvent the wheel; Why not stand on the shoulders of tech giants? [**Lower TCO**] Lower the upfront costs to begin implementing an idea; Shift costs from licensing proprietary packages to customization and implementation. By virtue of being distributed and unbridled by policy or technical debt, open source projects all but necessitate **modern software development workflows**. Everything is electronic and developed in the **open**, **multiple activities** are happening at once, and **testing** is being done by multiple parties at any one time. Throw in modern CI/CD tools like Gitlab, Travis, and others and much more can be automated.

+++
#### Cost Savings (cont.)
- The future

Note:
The future is in open source. Microsoft and Apple have opensourced their primary dev environments, and IBM, SAP, Adobe, and more are actively participating in different open source initiatives. Jim Zemlin, the Exec Director of the Linux Foundation, said this in his opening keynote for the Open Source Summit in September:

+++
![tweet](images/jzemlin_opensource_tweet.png)<br />
*Jim Zemlin, LinuxFoundation.org*

+++
#### Cost Savings (cont.)
- The future
- Patch on your schedule
- Upstream improvements <!-- .element: class="fragment" -->

Note:
[**Patch**] No waiting on proprietary software/company to decide when they'll patch something that is mission-critical to you and your business. [**Upstream**] There is benefit for you (when improvements happen and you include them in your software), and benefit for them (when you make improvements and submit back to the project). Everyone wins!

+++
#### Github's Co-Founder Tom Preston-Werner

- [Work]force multiplier
- Modular <!-- .element: class="fragment" -->
- Reduced duplication of effort <!-- .element: class="fragment" -->
- Great advertising <!-- .element: class="fragment" -->
- Attract talent <!-- .element: class="fragment" -->
- Best technical interview possible <!-- .element: class="fragment" -->

Note:
In a 2011 post, Github's Co-Founder talked about why to "Open Source (Almost) Everything". If your code is popular enough to attract outside contributions, you will have created a **workforce multiplier** that helps you get more work done faster and cheaper. Every bug fix and feature that you merge is time saved and customer frustration avoided. When you think about your code being opensourced, you're much more likely to make it **modular**; and visa versa - thus making it easier to integrate and support. By using (and creating) open source code we can drastically **reduce duplication of effort**. Less duplication means more resources towards things that matter. Opensourced software gets a lot of good karma in the industry, thus leading to **great advertising**. Smart people like to hang out with other smart people. Smart developers like to hang out with smart code. When you open source useful code, you **attract talent**. In the same way, hiring those who are already contributing provides the **best technical interview possible**.

+++
![puppies](images/puppies.gif)

Note:
We've now come to the spot in our talk where we pause for the obligatory puppy pic

+++
#### Moral Benefits
- Free speech, not beer
  - Run software <!-- .element: class="fragment" -->
  - Study and modify <!-- .element: class="fragment" -->
  - Redistribute <!-- .element: class="fragment" -->
  - Distribute modifications <!-- .element: class="fragment" -->
- Obligation to give back <!-- .element: class="fragment" -->
- Teach next generation <!-- .element: class="fragment" -->

Note:
Using open source software isn't without cost (in that free sense). When it's called free it's usually in reference to the rights the software consumers receive, not the cost they must pay. Adobe Flash = free to use, but non-free proprietary software. Free software refers to 4 core freedoms. Non-free software has a high potential to bring risks to the user as it's developed in private and the publisher controls it. You have a higher risk of spying, theft, etc. as a result - not to mention heightened security risks in the case of Flash. Free software places power back in the hands of users and ensures users control the software they use, not the other way around.
Open source is the give-a-penny-take-a-penny jar of software. If you consume open source, be it a server, a desktop publishing application, or a software library, you have an **obligation to give back** to the community - the Golden Rule of software. When software is open source, it allows for the **next generation to learn** and grow and start their career in giving back because of the example they have seen.

+++
### Favorite Open Source projects
- Android: largest mobile OS <!-- .element: class="fragment" -->
- Ubuntu: largest Linux-based desktop OS <!-- .element: class="fragment" -->
- Gitlab: software version control, and more <!-- .element: class="fragment" -->
- OmniROM: port of the Android Open Source Project (AOSP) <!-- .element: class="fragment" -->
- TODO Group: open source collaboration providing guides on open source in the workplace - todogroup.org <!-- .element: class="fragment" -->

Note:
I have a few open source projects which I am particularly partial to, each of which have a fantastic community around them.

---
### Open source is all about Community
- Benefit through, and because of, community <!-- .element: class="fragment" -->
- Generally only hindered by the number of contributors <!-- .element: class="fragment" -->
- Successful communities often accidental <!-- .element: class="fragment" -->
- Be intentional, make it easy to join and get involved <!-- .element: class="fragment" -->

Note:
Whether it's development or advocacy, open-source software and other collaborative projects benefit through, and because of, community. Unlike with traditional projects that require physical resources, sharing economies are generally only hindered by the number of people contributing to an effort and their ability to acquire and share knowledge. Oftentimes some of the most successful communities end up being accidental as the environment happens to be right. Similar to how some things just grow without any specific care given. But it's important to be intentional and make it easy to join and get involved in a community. Someone should be able to move from reading about your project or community to signing up and being put in a position to contribute as quickly as possible. Remove every possible obstacle to participation.

+++?image=images/dna.png&size=auto 90%
### What is Community?
#### A group of people with unique:
- Values <!-- .element: class="fragment" -->
- Personalities <!-- .element: class="fragment" -->
- Expertise <!-- .element: class="fragment" -->
- Interests <!-- .element: class="fragment" -->
- Humor <!-- .element: class="fragment" -->

Note:
So what is community? It is generally defined as a group of people with unique:

+++
#### Important Foundational Steps
- Setup Team Structure <!-- .element: class="fragment" -->
- Building the Environment <!-- .element: class="fragment" -->
- Facilitate Communication <!-- .element: class="fragment" -->
- Processes and Procedures <!-- .element: class="fragment" -->

Note:
We won't go too deeply into the more foundational steps, but I did want to mention them as I feel they are important.
Structure, Environment, Processes and Procedures, etc.
+++
![jono](images/artofcommunity.jpg)<br />
http://www.artofcommunityonline.org/<br />
Author: Jono Bacon

Note:
I encourage you to check out Jono Bacon's "Art of Community" which lays out a lot more of the steps needed in the early stages, things like mission, vision, meritocracy, etc.

+++?image=images/generational.png&size=auto 90%
### Lifecycle of Communities
- Inception <!-- .element: class="fragment" -->
- Establishment <!-- .element: class="fragment" -->
- Maturity <!-- .element: class="fragment" -->
- Mitosis <!-- .element: class="fragment" -->
- Death <!-- .element: class="fragment" -->

Note:
Feverbee, a community-consulting company, has come up with 4 stages in a community's lifecycle, with each having their own steps and/or tasks to get to the next stage. In the **INCEPTION** phase, you'll be inviting members to join, starting discussions and get participation in them, and build the relationships needed to make the community viable. When your community becomes **ESTABLISHED** you'll be writing content, organizing events, talks, etc., analyzing statistics about the community, deal with conflicts/disputes, etc. Community **MATURITY** will see the influence of the community growing, volunteers growing; goals and vision will be streamlined, refined, and widely adopted. **MITOSIS** is when a cell splits into new nuclei - or in this case forms more-focused sub groups in your community. Communities can be thought of as organisms. Organisms are born, they grow, they mature, some split and replicate themselves elsewhere, and then they die. It is natural, and if they aren't doing this, they're already dead - and with communities it is a real thing - and so I am adding **DEATH**.

+++
![death](images/death.jpg)

+++
#### A "Sense" of Community
- Membership <!-- .element: class="fragment" -->
- Identity <!-- .element: class="fragment" -->
- Influence <!-- .element: class="fragment" -->
- Attachment <!-- .element: class="fragment" -->

Note: As communities are formed, a sense of community is important for members to begin to have - it's the idea around which the community is experienced. They should have a feeling of belonging (**Membership**). This should move into their goals in the community matching others in the community (**Identity**). They'll stick around when they feel that they **influence** and are influenced by the community, and when they begin to share an emotional connection (**Attachment**) the bond with the community is complete.

+++
### Everything built on transparency and openness
- Transparency is essential to the formation, growth, and health of the community
- Should encompass everything, from process/procedures to decision-making <!-- .element: class="fragment" -->
- Puts everyone on the same level, where anything can be questioned, people can disagree and collaborate <!-- .element: class="fragment" -->
- Utilize wikis, issues, IRC/Slack/others <!-- .element: class="fragment" -->

Note:
Everything you do though needs to be wrapped up with **transparency and openness**. Great example of this is the Ubuntu Community and Gitlab. From the very onset of both they have strived to be open with their direction and decision making - **really everything**. This is very important as it builds trust which is vital to success. It also helps to **level the playing field** and make sure that everyone knows they have a role to play and are just as needed as the next person. There are a number of ways to achieve this, from **wikis, to Github issues, IRC, Slack, etc.**

+++?image=images/generational.png&size=auto 90%
### Community Involvement Lifecycle
- Lurker <!-- .element: class="fragment" -->
- Novice <!-- .element: class="fragment" -->
- Regular <!-- .element: class="fragment" -->
- Leader <!-- .element: class="fragment" -->
- Elder <!-- .element: class="fragment" -->

Note:
Communities are generational, and you will typically see them fit into these roles, with no pre-determined timeframe on each  "upgrade". At XDA this has been an interesting lifecycle. We don't restrict people from benefiting from the community (free downloads / **Lurker**), but we do require that they signup in order to participate - putting them immediately into that **"novice"** role. Some will immediately become a **regular**, while others will take awhile to make that leap. Eventually you will see **leaders** start to emerge, and these are those who embody the spirit of the community and who set the standards for participation. **Elders** are those who have been around awhile, and are now on their way out - usually due to things like job, family, lack of interest, new stage in life, etc.

+++?image=images/evolution.jpg&size=auto 90%

Note:
Community evolution is sort of like grabbing a bag of seeds and throwing them in the garden. The responsibility of the Community Manager is to water it and make sure the ground has the nutrients necessary to facilitate growth. You likely won't know what the outcome will be, but for me that is part of the excitement of community. **WATERMELON STORY**
Nothing happens though unless people participate.

+++
### Why Participate or Contribute?
"If you can *enable* an environment in which people can share, they will and the benefits will entice others to join." -**Susannah Fox**, *Pew Internet / American Life Project*

+++
### Why Participate or Contribute?
- Help to receive help <!-- .element: class="fragment" -->
- Increased recognition <!-- .element: class="fragment" -->
- Make an impact <!-- .element: class="fragment" -->
- Connection with others in community <!-- .element: class="fragment" -->
- Belonging <!-- .element: class="fragment" -->

Note:
It is important to understand the different reasons that people participate in communities. A few of the different ways are:

Understanding why people would want to participate in your community is essential, but it is not enough to stop there - you must provide incentives for them to participate and move through the different stages of participation.

+++
### Incentives for participation
- Recognition and status boost <!-- .element: class="fragment" -->
- Access to more info, tools, resources, etc. <!-- .element: class="fragment" -->
- More capabilities and control <!-- .element: class="fragment" -->
- Swag. Cool stuff. <!-- .element: class="fragment" -->

Note:
Being **recognized** for the things people do within the community, or boosting their status (sometimes both);
giving more **tools**, resources, or information;
more **capabilities or control**;
and **swag** (I mean who doesn't like swag?)
These are all are ways to bring about more participation. It's essential to figure out what works best for _your_ community - there isn't one size / fits all.

---
### The Story of XDA
- Began in Amsterdam Late 2003
- Group of friends wanting to modify these "XDA" devices from O2 <!-- .element: class="fragment" -->

+++?image=images/o2_xda.png&size=auto 90%

+++
### Exponential Growth
![growth](images/xda_growth_chart.png)

Note:
2006 - 162644 users; 2007 - 519813 users; 2008 - 1.2mil users (Android arrives on XDA); 2009 - 2mil users; 2010 - 3mil users; 2011 - 4.2mil users; 2012/13 - 5.4mil users; 2014/15 - 6.9mil users; 2016 - 7.8mil users; 2017 - 8.3mil users

As XDA grew, the staff and owners of the site had a love for the community - but to a large extent had let the site self-manage itself up until 2009. At that point, with 2mil+ users and only 20 moderators the Internet trolls had begun to take over the site. We had begun to see what was already evident - Growing Pains.

+++
### Pains of Exponential growth
- Growth in users brought out the worst in Internet humanity - the Troll.

+++?image=images/internet-troll.png&size=auto 90%
+++?image=images/desk-internet-troll.png&size=auto 90%

+++
### Pains of Exponential growth
- Growth in users brought out the worst in Internet humanity - the Troll.
- Android's phenomenal growth largely responsible for XDA growth.
- Younger demographic brought a different mentality, shifting from community-driven to self-centered <!-- .element: class="fragment" -->
- Growth in kanging the work of others and then begging for donations. <!-- .element: class="fragment" -->

Note:
With the growth of Android, and huge variety of devices both in price and options, spurred the growth XDA saw. It also brought a younger demographic to the site, along with it a different mentality. A growth in using the work of others without common courtesy being given, i.e. "kanging", caused many of XDA's stalwart devs to seek elsewhere to share their work.

+++
### XDA Response
- Due to the low numbers of moderators on the site, things quickly got out of control
- Senior staff established processes for how the community was to be run <!-- .element: class="fragment" -->
- Began recruitment of new moderators  <!-- .element: class="fragment" -->
- New programs created to provide incentives for participation <!-- .element: class="fragment" -->
- Emphasis on environments where devs can learn and collaborate <!-- .element: class="fragment" -->

+++
### Going Forward
- Continually take stock of community
- More incentives like tools and OEM collaboration  <!-- .element: class="fragment" -->
- Development of community-driven XDA Labs and XDA Feed <!-- .element: class="fragment" -->

+++?image=images/xdalabs1.png&size=auto 90%
<!-- .slide: data-background-transition="none" -->
+++?image=images/xdalabs2.png&size=auto 90%
<!-- .slide: data-background-transition="none" -->
+++?image=images/xdalabs3.png&size=auto 90%
<!-- .slide: data-background-transition="none" -->
+++?image=images/xdafeed1.png&size=auto 90%
<!-- .slide: data-background-transition="none" -->
+++?image=images/xdafeed2.png&size=auto 90%
<!-- .slide: data-background-transition="none" -->

+++
### Going Forward
- Continually take stock of community
- More incentives like tools and OEM collaboration
- Development of community-driven XDA Labs and XDA Feed
- Push for more transparency <!-- .element: class="fragment" -->

---
### Additional Resources
- https://www.linuxfoundation.org/resources/open-source-guides/
- Open Source Initiative - http://opensource.org
- CHAOSS: Community Health Analytics Open Source Software - http://chaoss.community
- *Art of Community*, Jono Bacon, http://artofcommunityonline.org
- TODO Group: open source collaboration providing guides on open source in the workplace - http://todogroup.org

---
## Q&A


---
## Thank You!

Follow me at <i class="fa fa-twitter"></i> @jerdogxda!<br />

Let me know your feedback - https://goo.gl/QvwRPX!
